package com.sotoon.findnearplaces.data.datasource

import com.sotoon.findnearplaces.data.datasource.base.LocalDataSource
import com.sotoon.findnearplaces.domain.Model

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
interface PlacesLocalDataSource:LocalDataSource<Model> {
}
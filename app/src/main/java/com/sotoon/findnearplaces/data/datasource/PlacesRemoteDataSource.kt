package com.sotoon.findnearplaces.data.datasource

import com.sotoon.findnearplaces.data.datasource.base.RemoteDataSource
import com.sotoon.findnearplaces.domain.Model
import com.sotoon.findnearplaces.domain.NetworkResponse

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
interface PlacesRemoteDataSource : RemoteDataSource<Model> {

    suspend fun getPlaces(lat:Float,lon:Float): NetworkResponse<Model.BaseResult>

    suspend fun getPlace(id:String):NetworkResponse<Model.BaseResult>
}
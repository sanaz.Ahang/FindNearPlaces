package com.sotoon.findnearplaces.data.datasource.base

import com.sotoon.findnearplaces.domain.Model

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
interface DataSource<T: Model>
package com.sotoon.findnearplaces.di

import com.sotoon.findnearplaces.network.api.PlacesAPI
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
val networkModule = module {
    single { provideMoshiConvertorFactory() }
    single { provideHttpLoggingInterceptor() }
    single { provideOkhttpClient(get()) }
    single { provideRetrofit(get(), get()) }

    factory { providePlacesApi(get()) }
}

private fun provideMoshiConvertorFactory(): MoshiConverterFactory =
    MoshiConverterFactory.create()

private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor =
    HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

private fun provideOkhttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
    OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .readTimeout(20, TimeUnit.SECONDS)
        .writeTimeout(20, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .build()

private fun provideRetrofit(
    okHttpClient: OkHttpClient,
    moshiConverterFactory: MoshiConverterFactory
): Retrofit =
    Retrofit.Builder()
        .baseUrl("https://api.tomtom.com/")
        .client(okHttpClient)
        .addConverterFactory(moshiConverterFactory)
        .build()

private fun providePlacesApi(retrofit: Retrofit): PlacesAPI =
    retrofit.create(PlacesAPI::class.java)

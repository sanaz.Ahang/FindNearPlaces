package com.sotoon.findnearplaces.network.datasource

import com.sotoon.findnearplaces.domain.Model
import com.sotoon.findnearplaces.domain.NetworkResponse
import com.sotoon.findnearplaces.network.api.Dto
import retrofit2.Response

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
inline fun <T> execute(request: () -> Response<T>): NetworkResponse<T> =
    try {
        val response = request()
        if (response.isSuccessful) {
            NetworkResponse.Success(response.body()!!)
        } else {
            NetworkResponse.Failure(response.code())
        }
    } catch (ex: Exception) {
        NetworkResponse.Error(ex)
    }

fun Dto.BaseResult.mapToModel() = Model.BaseResult(
    summery = summery.mapToModel(),
    results = results.map {
        it.mapToModel()
    }
)

fun Dto.Summery.mapToModel() = Model.Summery(
    offset = offset,
    numResults = numResults
)

fun Dto.Result.mapToModel() = Model.Result(
    id = id,
    name = name,
    phone = phone
)

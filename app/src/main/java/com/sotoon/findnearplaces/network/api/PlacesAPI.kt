package com.sotoon.findnearplaces.network.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Sanaz Ahang on 08,September,2022
 */

//Api key:lTOEZi7JscjTFwsK9shcVbOi4zWbKT92
interface PlacesAPI {
    @GET("search/2/nearbySearch/.json?key=lTOEZi7JscjTFwsK9shcVbOi4zWbKT92")
    suspend fun getPlaces(
        @Query("lat") lat: Float,
        @Query("lon") Long: Float):Response<Dto.BaseResult>

    @GET("search/2/place.json?key=lTOEZi7JscjTFwsK9shcVbOi4zWbKT92")
        suspend fun getPlaceDetail(
        @Query("entityId") entityId:String
        ):Response<Dto.BaseResult>
}
package com.sotoon.findnearplaces.network.api

import com.squareup.moshi.Json

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
sealed class Dto {

    data class BaseResult(
        @Json(name = "summary") val summery: Summery,
        @Json(name = "results") val results: List<Result>

    ) : Dto()

    data class Summery(
        @Json(name = "offset") val offset: Int,
        @Json(name = "numResults") val numResults: Int,

        ) : Dto()

    data class Result(
        @Json(name = "id") val id: String,
        @Json(name = "name") val name: String,
        @Json(name = "phone") val phone: String

    ) : Dto()
}


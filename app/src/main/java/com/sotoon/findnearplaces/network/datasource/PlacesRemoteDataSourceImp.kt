package com.sotoon.findnearplaces.network.datasource

import com.sotoon.findnearplaces.data.datasource.PlacesRemoteDataSource
import com.sotoon.findnearplaces.domain.Model
import com.sotoon.findnearplaces.domain.NetworkResponse
import com.sotoon.findnearplaces.domain.map
import com.sotoon.findnearplaces.network.api.Dto
import com.sotoon.findnearplaces.network.api.PlacesAPI

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
class PlacesRemoteDataSourceImp(private val placesAPI: PlacesAPI) : PlacesRemoteDataSource {

    override suspend fun getPlaces(lat: Float, lon: Float): NetworkResponse<Model.BaseResult> =
        execute {
            placesAPI.getPlaces(lat, lon)
        }.map(Dto.BaseResult::mapToModel)


    override suspend fun getPlace(id: String): NetworkResponse<Model.BaseResult> =
        execute {
            placesAPI.getPlaceDetail(id)
        }.map(Dto.BaseResult::mapToModel)
}
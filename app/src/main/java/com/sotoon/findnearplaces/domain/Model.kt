package com.sotoon.findnearplaces.domain

import com.sotoon.findnearplaces.network.api.Dto

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
sealed class Model {

    data class BaseResult(
        val summery: Summery,
        val results: List<Result>
    ) : Model()

    data class Summery(
        val offset: Int,
        val numResults: Int,
    ) : Model()

    data class Result(
        val id: String,
        val name: String,
        val phone: String
    ) : Model()

}


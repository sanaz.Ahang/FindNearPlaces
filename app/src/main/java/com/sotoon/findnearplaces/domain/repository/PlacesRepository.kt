package com.sotoon.findnearplaces.domain.repository

import com.sotoon.findnearplaces.domain.Model

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
interface PlacesRepository : Repository<Model> {

}
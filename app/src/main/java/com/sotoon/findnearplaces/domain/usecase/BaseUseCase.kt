package com.sotoon.findnearplaces.domain.usecase

/**
 * Created by Sanaz Ahang on 08,September,2022
 */
interface BaseUseCase<inPut, outPut> {
    suspend fun invoke(input: inPut): outPut
}